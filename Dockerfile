FROM ruby:3.2.4-slim

RUN apt-get update && apt-get install -y \
    build-essential gcc make \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /app

COPY . .
RUN bundle config set --local deployment 'true' \
    && bundle config set --local without 'development test' \
    && bundle install
