# La Salle du Temps 

* Développeur.
* Blogueur.
* Roi du rire en tout genre.
* Bref, couteau-suisse du clavier.


# How to build
(Optional) if your architecture is not into this gemfile.lock:
```bash
docker run --rm -v "$(pwd)":/app -w /app ruby:3.2.4-alpine sh -c "apk add --no-cache build-base && bundle lock --add-platform x86_64-linux"
```

Then:
```bash 
docker compose up -d
```

# How to dev
```bash 
bundle install  
bundle exec jekyll serve  
```

# How to publish
```bash
# Write your awesome post into _posts then
docker compose build lasalledutemps
docker compose up -d lasalledutemps
```
